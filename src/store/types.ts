import { IUserState } from '@/app/User/types/user/user'
import {
    IURLState,
    ICategoriesState,
} from '@/app/Manager/categories/store/types'
import { ISelectionState } from '@/app/User/types/selection/selection'
import { IAdState } from '@/app/Sales/Ads/types/ad'

export interface RootState {
    version: string
    User?: IUserState
    URLS?: IURLState
    Categories?: ICategoriesState
    Selection?: ISelectionState
    Ads?: IAdState
}
