import { ActionTree } from 'vuex'
import { IUserState } from '../../types/user/user'
import { RootState } from '@/store/types'
import { offlineDB } from '@/app/User/offline/db'
import { ISelection } from '../../types/selection/selection'
import { INativeWeights } from '../../types/user/weight'
import { INativeCapacity } from '../../types/user/capacity'

export const actions: ActionTree<IUserState, RootState> = {
    saveUserSelection({ commit }, payload: ISelection) {
        return new Promise(async (resolve, reject) => {
            try {
                commit('addUserSelection', payload)
                if (payload.weights === undefined) {
                    resolve()
                    return
                }
                if (payload.capacity === undefined) {
                    resolve()
                    return
                }
                await Promise.all([
                    offlineDB.saveAllWeights(payload.weights),
                    offlineDB.saveAllCapacities(payload.capacity),
                ])
                resolve()
            } catch (error) {
                console.log(error)
                reject()
            }
        })
    },
    getOfflineSelection({ commit }) {
        return new Promise(async (resolve, reject) => {
            try {
                const [capacities, weights] = await Promise.all([
                    offlineDB.capacity,
                    offlineDB.weights,
                ])

                if (capacities === undefined || weights === undefined) {
                    resolve()
                    return
                }

                const newWeights: INativeWeights = {}
                weights.forEach(weight => {
                    if (newWeights[weight.category] === undefined) {
                        newWeights[weight.category] = []
                    }
                    newWeights[weight.category].push(weight)
                })

                const newCapacities: INativeCapacity = {}
                capacities.forEach(capacity => {
                    newCapacities[capacity.name as keyof INativeCapacity] =
                        capacity.capacity
                })

                commit('addUserSelection', {
                    capacity: newCapacities,
                    weights: newWeights,
                })
                resolve()
            } catch (error) {
                console.log(error)
                reject()
            }
        })
    },
}
