import { ActionTree } from 'vuex'
import { IUserState, IUser } from '../../types/user/user'
import { RootState } from '@/store/types'
import { offlineDB } from '@/app/User/offline/db'
import { Auth } from '@/app/Auth/auth.service'

export const actions: ActionTree<IUserState, RootState> = {
    getUser({ commit, state }) {
        return new Promise(async (resolve, reject) => {
            try {
                const user = await offlineDB.user
                if (user === undefined) {
                    resolve()
                    return
                }
                commit('addUser', user)
                Auth.getUserFromDB()
                resolve()
            } catch (error) {
                console.log(error)
                reject()
            }
        })
    },
    saveGuestUser({ commit }, payload: IUser) {
        return new Promise(async (resolve, reject) => {
            try {
                await offlineDB.saveUser(payload)
                await Auth.getUserFromDB()
                commit('addUser', payload)
                resolve(true)
            } catch (error) {
                console.log(error)
                reject(false)
            }
        })
    },
}
