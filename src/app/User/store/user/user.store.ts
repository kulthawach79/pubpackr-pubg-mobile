import { Module } from 'vuex'
import { RootState } from '@/store/types'
import { IUserState } from '../../types/user/user'
import { mutations } from './mutations'
import { actions } from './actions'

export const state: IUserState = {
    user: undefined,
    subscription: {
        status: false,
    },
    error: false,
}

const namespaced: boolean = true

export const User: Module<IUserState, RootState> = {
    namespaced,
    state,
    actions,
    mutations,
}
