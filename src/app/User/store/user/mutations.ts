import { MutationTree } from 'vuex'
import { IUserState, IUser } from '../../types/user/user'

export const mutations: MutationTree<IUserState> = {
    addUser(state: IUserState, payload: IUser) {
        state.user = payload
    },
}
