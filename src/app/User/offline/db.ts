import Dexie from 'dexie'
import { IUser } from '../types/user/user'
import { IWeight, INativeWeights, IAppWeight } from '../types/user/weight'
import { ICapacity, INativeCapacity } from '../types/user/capacity'

class MyDatabase extends Dexie {
    user: Dexie.Table<IUser, number>
    weights: Dexie.Table<IWeight, number>
    capacity: Dexie.Table<ICapacity, number>

    constructor() {
        super('PubPackr')

        this.version(1).stores({
            user: '++id, firstName',
            weights: '++id, name, category',
            capacity: '++id, name, category',
        })

        this.user = this.table('user')
        this.weights = this.table('weights')
        this.capacity = this.table('capacity')
    }
}

class MyUser {
    private user: Dexie.Table<IUser, number>

    constructor(db: MyDatabase) {
        this.user = new MyDatabase().user
    }

    listenToNewUser(callback: (user: IUser) => void) {
        this.user.hook('creating', function(primKey, obj, transaction) {
            console.log(obj, 'listen')
            callback(obj as IUser)
        })
    }

    subscribe(callback: (user: IUser) => void) {
        this.user.hook(
            'updating',
            (modifications, primKey, obj, transaction) => {
                callback(modifications as IUser)
            }
        )
    }

    unsubscribe(callback: (user: IUser | undefined) => void) {
        this.user.hook('updating').unsubscribe(callback)
    }
}

class OfflineDB {
    private db: MyDatabase
    public USER: MyUser

    constructor() {
        this.db = new MyDatabase()
        this.USER = new MyUser(this.db)
    }

    // Getters
    get user() {
        return (async () => {
            try {
                const user = await this.db.user.get(1)
                return user
            } catch (error) {
                console.log(error)
                return
            }
        })()
    }

    get weights() {
        return (async () => {
            try {
                const weights = await this.db.weights.toArray()
                return weights
            } catch (error) {
                console.log(error)
                return
            }
        })()
    }

    get capacity() {
        return (async () => {
            try {
                const capacities = await this.db.capacity.toArray()
                return capacities
            } catch (error) {
                console.log(error)
                return
            }
        })()
    }

    // METHODS
    //1. User Methods
    async saveUser(user: IUser): Promise<void> {
        try {
            const currentUser = await this.user
            if (currentUser !== undefined) {
                if (currentUser.id) {
                    await this.updateUser(user, currentUser.id)
                } else {
                    console.log('ERROR: Unindexed Offline User')
                }
            } else {
                await this.db.user.put(user)
            }
            return
        } catch (error) {
            console.log(error)
            return
        }
    }

    async updateUser(changes: IUser, index: number): Promise<void> {
        try {
            await this.db.user.update(index, changes)
            return
        } catch (error) {
            console.log(error)
            return
        }
    }

    async deleteUser(): Promise<void> {
        try {
            await this.db.user.delete(1)
            return
        } catch (error) {
            console.log(error)
            return
        }
    }

    //2. Selection Methods
    // 2.1 Weights
    async getWeight(name: string): Promise<IWeight | undefined> {
        try {
            const weight = await this.db.weights.get({ name })
            return weight
        } catch (error) {
            console.log(error)
            return undefined
        }
    }

    async deleteWeight(payload: IWeight | IAppWeight): Promise<void> {
        try {
            const existingWeight = await this.getWeight(payload.name)
            if (existingWeight === undefined) {
                throw "Error in Delete Weight: Record To Delete Doesn't Exist"
            }
            if (existingWeight.id == undefined) {
                throw "Error in Delete Weight: Record To Delete Doesn't Have an ID"
            }
            await this.db.weights.delete(existingWeight.id)
        } catch (error) {
            console.log(error)
            return
        }
    }

    async updateWeight(payload: IWeight): Promise<void> {
        try {
            const existingWeight = await this.getWeight(payload.name)
            if (existingWeight === undefined) {
                throw "Error in UpdateWeight: Record To Update Doesn't Exist"
            }
            if (existingWeight.id == undefined) {
                throw "Error in UpdateWeight: Record To Update Doesn't Have an ID"
            }
            await this.db.weights.update(existingWeight.id, payload)
            return
        } catch (error) {
            console.log(error)
            return
        }
    }

    async saveAllWeights(weights: INativeWeights): Promise<void> {
        try {
            const data: IWeight[] = []

            for (const category in weights) {
                if (weights.hasOwnProperty(category)) {
                    const items = weights[category] as IAppWeight[]
                    for (let index = 0; index < items.length; index++) {
                        const weight = items[index]
                        const existingWeight = await this.getWeight(weight.name)
                        if (existingWeight === undefined) {
                            if (weight.number > 0) {
                                data.push({
                                    name: weight.name,
                                    category,
                                    number: weight.number,
                                })
                            } else {
                                this.deleteWeight(weight)
                            }
                        } else {
                            if (weight.number > 0) {
                                this.updateWeight({
                                    name: weight.name,
                                    category,
                                    number: weight.number,
                                })
                            } else {
                                this.deleteWeight(weight)
                            }
                        }
                    }
                }
            }

            const tx = this.db.transaction('rw', this.db.weights, async () => {
                await this.db.weights.bulkPut(data)
            })
            return
        } catch (error) {
            console.log(error)
            return
        }
    }

    // 2.2 Capacity
    async getCapacity(name: string): Promise<ICapacity | undefined> {
        try {
            const capacity = await this.db.capacity.get({ name })
            return capacity
        } catch (error) {
            console.log(error)
            return undefined
        }
    }

    async updateCapacity(payload: ICapacity): Promise<void> {
        try {
            const existingCapacity = await this.getCapacity(payload.name)
            if (existingCapacity === undefined) {
                throw "Error in UpdateCapacity: Record To Update Doesn't Exist"
            }
            if (existingCapacity.id == undefined) {
                throw "Error in UpdateCapacity: Record To Update Doesn't Have an ID"
            }
            await this.db.capacity.update(existingCapacity.id, payload)
            return
        } catch (error) {
            console.log(error)
            return
        }
    }

    async saveAllCapacities(capacities: INativeCapacity): Promise<void> {
        try {
            const data: ICapacity[] = []

            for (const name in capacities) {
                if (capacities.hasOwnProperty(name)) {
                    const capacity = capacities[
                        name as keyof INativeCapacity
                    ] as number
                    const existingCapacity = await this.getCapacity(name)
                    if (existingCapacity === undefined) {
                        data.push({
                            name,
                            capacity,
                        })
                    } else {
                        this.updateCapacity({
                            name,
                            capacity,
                        })
                    }
                }
            }

            const tx = this.db.transaction('rw', this.db.capacity, async () => {
                await this.db.capacity.bulkPut(data)
            })
            return
        } catch (error) {
            console.log(error)
            return
        }
    }
}

export const offlineDB = new OfflineDB()
