interface IItem {
    name: string
    number: number
    unitWeight: number
    weight: number
}

interface IWeight {
    [key: string]: any
    barrel?: IItem[]
    scopes?: IItem[]
    stocks?: IItem[]
    muzzle?: IItem[]
    throwables?: IItem[]
    bullets?: IItem[]
    magazines?: IItem[]
    medkits: IItem[]
    boost: IItem[]
}

interface ICapacity {
    // [key: string]: any
    body?: number
    belt?: number
    vest?: number
    backpack?: number
}

export interface ISelection {
    weights: IWeight | undefined
    capacity: ICapacity | undefined
}
export interface ISelectionState {
    selection?: ISelection
    error: boolean
}
