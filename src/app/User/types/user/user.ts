export interface IUser {
    id?: number
    firstName: string
    lastName?: string
}
export interface IUserState {
    user?: IUser
    subscription?: {
        expire?: Date
        status: boolean
    }
    error: boolean
}
