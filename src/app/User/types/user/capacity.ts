export interface ICapacity {
    id?: number
    name: string
    capacity: number
}

export interface INativeCapacity {
    // [k in keyof x]: any
    body?: number
    belt?: number
    vest?: number
    backpack?: number
}
