export interface ICapacity {
    name: string
    capacity: number
}

export interface IItem {
    name: string
    weight: number
}

export interface ICategories {
    [key: string]: IItem[] | ICapacity[]
    medkits: IItem[]
    boost: IItem[]
    vests: ICapacity[]
    stocks: IItem[]
    barrel: IItem[]
    muzzle: IItem[]
    scopes: IItem[]
    throwables: IItem[]
    backpacks: ICapacity[]
    magazines: IItem[]
    bullets: IItem[]
    character: ICapacity[]
}

export interface ICategoriesState {
    categories: ICategories
    error: boolean
}

export interface IURL {
    name: string
    fileName: string
    url: string | null
}

export interface IURLDatabase {
    fileName: string
    filePath: string
    url: string
}

export interface IURLPayload {
    category: string
    index: number
    url: string
}

export interface IURLs {
    [key: string]: IURL[]
    medkits: IURL[]
    boost: IURL[]
    vests: IURL[]
    stocks: IURL[]
    barrel: IURL[]
    muzzle: IURL[]
    scopes: IURL[]
    throwables: IURL[]
    backpacks: IURL[]
    magazines: IURL[]
    bullets: IURL[]
    character: IURL[]
}

export interface IURLState {
    urls: IURLs
    error: boolean
}
