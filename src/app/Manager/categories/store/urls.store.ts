import { MutationTree, ActionTree, Module } from 'vuex'
import { RootState } from '@/store/types'
import { IURLs, IURLState, IURLPayload, IURLDatabase, IURL } from './types'
import { db } from '@/backend/firebase'

const urls: IURLs = {
    medkits: [
        {
            name: 'Bandages',
            fileName: 'bandage.svg',
            url: null,
        },
        {
            name: 'First Aid Kit',
            fileName: 'first-aid-kit.svg',
            url: null,
        },
        {
            name: 'Medkit',
            fileName: 'medkit.svg',
            url: null,
        },
    ],
    boost: [
        {
            name: 'Energy Drink',
            fileName: 'energy-drink.svg',
            url: null,
        },
        {
            name: 'Painkiller',
            fileName: 'painkiller.svg',
            url: null,
        },
        {
            name: 'Adrenaline Syringe',
            fileName: 'adrenaline-syringe.svg',
            url: null,
        },
    ],
    bullets: [
        {
            name: '9mm',
            fileName: '99mm.svg',
            url: null,
        },
        {
            name: '0.45 ACP',
            fileName: '45acp.svg',
            url: null,
        },
        {
            name: '5.56mm',
            fileName: '556.svg',
            url: null,
        },
        {
            name: '7.62mm',
            fileName: '762.svg',
            url: null,
        },
        {
            name: '0.300 Magnum',
            fileName: 'magnum.svg',
            url: null,
        },
        {
            name: '12 Gauge Shotgun Shell',
            fileName: '12-gauge.svg',
            url: null,
        },
        {
            name: 'Crossbow Bolt',
            fileName: 'bolt.svg',
            url: null,
        },
        {
            name: 'Flare',
            fileName: 'flare.svg',
            url: null,
        },
    ],
    magazines: [
        {
            name: 'Extended Quickdraw Mag (Pistols, SMG)',
            fileName: 'extended-quick-smg.svg',
            url: null,
        },
        {
            name: 'Extended Mag (Pitols, SMG)',
            fileName: 'extended-smg.svg',
            url: null,
        },
        {
            name: 'Quickdraw Mag (Pitols, SMG)',
            fileName: 'quick-smg.svg',
            url: null,
        },
        {
            name: 'Extended Quickdraw Mag (AR, DMR)',
            fileName: 'extended-quick-ar.svg',
            url: null,
        },
        {
            name: 'Extended Mag (AR, DMR)',
            fileName: 'extended-ar.svg',
            url: null,
        },
        {
            name: 'Quickdraw Mag (AR, DMR)',
            fileName: 'quick-ar.svg',
            url: null,
        },
        {
            name: 'Extended Quickdraw Mag (Sniper, DMR)',
            fileName: 'extended-quick-sniper.svg',
            url: null,
        },
        {
            name: 'Extended Mag (Sniper, DMR)',
            fileName: 'extended-sniper.svg',
            url: null,
        },
        {
            name: 'Quickdraw Mag (Sniper, DMR)',
            fileName: 'quick-sniper.svg',
            url: null,
        },
    ],
    throwables: [
        {
            name: 'Frag Grenade',
            fileName: 'frag-grenade.svg',
            url: null,
        },
        {
            name: 'Stun Grenade',
            fileName: 'stun-grenade.svg',
            url: null,
        },
        {
            name: 'Smoke Grenade',
            fileName: 'smoke-grenade.svg',
            url: null,
        },
        {
            name: 'Molotov Coctail',
            fileName: 'molotov.svg',
            url: null,
        },
        {
            name: 'Gas Can',
            fileName: 'gas-can.svg',
            url: null,
        },
    ],
    muzzle: [
        {
            name: 'Pistol/ SMG Suppressor',
            fileName: 'suppressor-smg.svg',
            url: null,
        },
        {
            name: 'AR/ DMR Suppressor',
            fileName: 'suppressor-ar.svg',
            url: null,
        },
        {
            name: 'Sniper/ DMR Suppressor',
            fileName: 'suppressor-sniper.svg',
            url: null,
        },
        {
            name: 'Pistol/ SMG Flash Hider',
            fileName: 'flash-hider-smg.svg',
            url: null,
        },
        {
            name: 'AR/ DMR Flash Hider',
            fileName: 'flash-hider-ar.svg',
            url: null,
        },
        {
            name: 'Sniper/ DMR Flash Hider',
            fileName: 'flash-hider-sniper.svg',
            url: null,
        },
        {
            name: 'Pistol/ SMG Compensator',
            fileName: 'compensator-smg.svg',
            url: null,
        },
        {
            name: 'AR/ DMR Compensator',
            fileName: 'compensator-ar.svg',
            url: null,
        },
        {
            name: 'Sniper/ DMR Compensator',
            fileName: 'compensator-sniper.svg',
            url: null,
        },
        {
            name: 'Choke',
            fileName: 'choke.svg',
            url: null,
        },
        {
            name: 'Duckbill',
            fileName: 'duckbill.svg',
            url: null,
        },
    ],
    stocks: [
        {
            name: 'Shotgun/ Sniper Bullet Loops',
            fileName: 'bullet-loops.svg',
            url: null,
        },
        {
            name: 'Sniper Cheek Pad',
            fileName: 'cheek-pad.svg',
            url: null,
        },
        {
            name: 'Micro Uzi Stock',
            fileName: 'stock.svg',
            url: null,
        },
        {
            name: 'M416/ Vector/ MP5K Tactical Stock',
            fileName: 'tactical-stock.svg',
            url: null,
        },
    ],
    scopes: [
        {
            name: 'Red Dot',
            fileName: 'red-dot.svg',
            url: null,
        },
        {
            name: 'Holographic',
            fileName: 'holographic.svg',
            url: null,
        },
        {
            name: '2x',
            fileName: '2x.svg',
            url: null,
        },
        {
            name: '3x',
            fileName: '3x.svg',
            url: null,
        },
        {
            name: '4x',
            fileName: '4x.svg',
            url: null,
        },
        {
            name: '6x',
            fileName: '6x.svg',
            url: null,
        },
        {
            name: '8x',
            fileName: '8x.svg',
            url: null,
        },
    ],
    barrel: [
        {
            name: 'Angled Foregrip',
            fileName: 'angled-foregrip.svg',
            url: null,
        },
        {
            name: 'Vertical Foregrip',
            fileName: 'vertical-foregrip.svg',
            url: null,
        },
        {
            name: 'Half Grip',
            fileName: 'half-grip.svg',
            url: null,
        },
        {
            name: 'Light Grip',
            fileName: 'light-grip.svg',
            url: null,
        },
        {
            name: 'Thumb Grip',
            fileName: 'thumb-grip.svg',
            url: null,
        },
        {
            name: 'Laser Sight',
            fileName: 'laser-sight.svg',
            url: null,
        },
        {
            name: 'Quiver for Crossbow',
            fileName: 'quiver.svg',
            url: null,
        },
    ],
    backpacks: [
        {
            name: 'No Backpack',
            fileName: 'no-backpack.svg',
            url: null,
        },
        {
            name: 'Level 1',
            fileName: 'level-1.svg',
            url: null,
        },
        {
            name: 'Level 2',
            fileName: 'level-2.svg',
            url: null,
        },
        {
            name: 'Level 3',
            fileName: 'level-3.svg',
            url: null,
        },
    ],
    vests: [
        {
            name: 'No Vest',
            fileName: 'no-vest.svg',
            url: null,
        },
        {
            name: 'Bulletproof Vest',
            fileName: 'bulletproof.svg',
            url: null,
        },
    ],
    character: [
        {
            name: 'Male',
            fileName: 'male.svg',
            url: null,
        },
        {
            name: 'Female',
            fileName: 'female.svg',
            url: null,
        },
    ],
}

const namespaced: boolean = true

const state: IURLState = {
    urls: urls,
    error: false,
}

const mutations: MutationTree<IURLState> = {
    updateURL(state: IURLState, payload: IURLPayload) {
        state.urls[payload.category][payload.index].url = payload.url
    },
}
const actions: ActionTree<IURLState, RootState> = {
    getURLS({ commit, state }) {
        return new Promise(async (resolve, reject) => {
            const { urls } = state

            const helperArr = (
                arr: Array<IURL>,
                img: IURLDatabase,
                category: string
            ) => {
                for (const [index, obj] of arr.entries()) {
                    if (obj.fileName === img.fileName) {
                        const payload: IURLPayload = {
                            category: category,
                            index: index,
                            url: img.url,
                        }
                        commit('updateURL', payload)
                    }
                }
            }

            const helperObj = (img: IURLDatabase) => {
                for (const key in urls) {
                    if (urls.hasOwnProperty(key)) {
                        const arr = urls[key]
                        helperArr(arr, img, key)
                    }
                }
            }
            try {
                // const images = await db.collection('images').get()

                // images.forEach(doc => {
                //     const img: any = doc.data()
                //     helperObj(img)
                // })

                db.collection('images').onSnapshot(images => {
                    images.forEach(doc => {
                        const img: any = doc.data()
                        helperObj(img)
                    })
                    resolve()
                })
            } catch (error) {
                console.log(error)
                reject()
            }
        })
    },
}

export const URLS: Module<IURLState, RootState> = {
    namespaced,
    state,
    mutations,
    actions,
}
