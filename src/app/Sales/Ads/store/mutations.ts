import { MutationTree } from 'vuex'
import { IAdState, IAd } from '../types/ad'

export const mutations: MutationTree<IAdState> = {
    addAd(state: IAdState, payload: IAd) {
        if (state.ads === undefined) state.ads = []
        state.ads.push(payload)
    },
}
