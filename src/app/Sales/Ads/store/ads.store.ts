import { Module } from 'vuex'
import { RootState } from '@/store/types'
import { IAdState, IAd } from '../types/ad'
import { mutations } from './mutations'
import { actions } from './actions'

const defaultAds: IAd[] = [
    {
        name: 'brave',
        title: 'Get Brave Browser',
        url: 'https://brave.com/pub729',
        posterImgUrl:
            'https://upload.wikimedia.org/wikipedia/en/thumb/6/68/Brave_logo.svg/1920px-Brave_logo.svg.png',
        popupImgUrl:
            'https://upload.wikimedia.org/wikipedia/en/thumb/6/68/Brave_logo.svg/1920px-Brave_logo.svg.png',
        videoUrl: 'https://www.youtube.com/watch?v=kLiLOkzLetE',
    },
]

export const state: IAdState = {
    ads: defaultAds,
    error: false,
}

const namespaced: boolean = true

export const Ads: Module<IAdState, RootState> = {
    namespaced,
    state,
    actions,
    mutations,
}
