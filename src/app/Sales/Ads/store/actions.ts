import { ActionTree } from 'vuex'
import { IAdState, IAd } from '../types/ad'
import { RootState } from '@/store/types'

export const actions: ActionTree<IAdState, RootState> = {
    getAds({ commit }, payload: IAd) {
        return new Promise(async (resolve, reject) => {
            try {
                resolve()
            } catch (error) {
                console.log(error)
                reject()
            }
        })
    },
}
