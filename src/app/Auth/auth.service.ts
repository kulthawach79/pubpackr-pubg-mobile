import { offlineDB } from '@/app/User/offline/db'
import { Store } from 'vuex'
import store from '@/store/store'
import { RootState } from '@/store/types'
import { IUserState, IUser } from '../User/types/user/user'

class AuthService {
    private USER_STATE: IUserState | undefined
    private USER_OFFLINE: IUser | undefined

    constructor(store: Store<RootState>) {
        this.USER_STATE = store.state.User
    }

    get currentUser(): IUser | null {
        if (this.USER_STATE === undefined) return null
        if (
            this.USER_STATE.user != undefined &&
            this.USER_OFFLINE != undefined
        ) {
            return this.USER_STATE.user
        } else {
            return null
        }
    }

    async getUserFromDB(): Promise<void> {
        const user = await offlineDB.user
        this.USER_OFFLINE = user
    }
}

export const Auth = new AuthService(store)
