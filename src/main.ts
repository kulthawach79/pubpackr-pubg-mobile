import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import './vendor/tailwind.scss'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
    faPlus,
    faSync,
    faCheckCircle,
    faMinus,
    faChevronDown,
    faHeart,
    faUserAstronaut,
    faChevronLeft,
} from '@fortawesome/free-solid-svg-icons'
import Vuelidate from 'vuelidate'

import './registerServiceWorker'

Vue.use(Vuelidate)

library.add(
    faPlus,
    faSync,
    faCheckCircle,
    faMinus,
    faChevronDown,
    faChevronLeft,
    faHeart,
    faUserAstronaut
)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
