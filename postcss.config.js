const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const purgecss = require('@fullhuman/postcss-purgecss');

module.exports = ({ webpack }) => ({
  plugins: [
    tailwindcss,
    webpack.mode === 'production'
      ? purgecss({
        content: ['./src/**/*.vue'],
        extractors: [
          {
            extractor: class TailwindExtractor {
              static extract(content) {
                // return content.match(/[A-z0-9-:\/]+/g) || [];
                return content.match(/[\w-/:]+(?<!:)/g) || [];
              }
            },
            extensions: ['vue']
          }
        ]
      })
      : '',
    autoprefixer,
  ]
});