// Inside vue.config.js
const path = require('path');
const PrerenderSPAPlugin = require('prerender-spa-plugin');
module.exports = {
    // ...other vue-cli plugin options...
    pwa: {
        name: 'PubPackr',
        themeColor: '#ED8936',
        msTileColor: '#ECC94B',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black',
        iconPaths: {},

        // configure the workbox plugin
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            // swSrc is required in InjectManifest mode.
            swSrc: 'src/offline/sw.js',
            swDest: 'service-worker.js',
        }
    },
    configureWebpack: {
        plugins: [
            new PrerenderSPAPlugin({
                // Required - The path to the webpack-outputted app to prerender.
                staticDir: path.join(__dirname, 'dist'),
                // Required - Routes to render.
                routes: ['/'],
            })
        ]
    }
};
